import java.util.Scanner;
public class Library {
    private static Book[] books = new Book[20];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int isEnd = 0;
        do {
            System.out.println("What do you want?");
            System.out.println("1) Add book");
            System.out.println("2) Show book");
            System.out.println("3) Show all books");
            System.out.println("4) Delete book");
            System.out.println("5) Turn off");
            System.out.println("Write a number of option");

            switch (scanner.nextInt()) {
                case 1:
                    bookAdd(scanner);
                    break;
                case 2:
                    bookShow(scanner);
                    break;
                case 3:
                    bookShowAll();
                    break;
                case 4:
                    bookDelete(scanner);
                    break;
                case 5:
                    isEnd++;
                    break;
                default:
                    System.out.println("Error, try again");
                    break;
            }
        } while (isEnd == 0);
    }

    private static void bookAdd(Scanner input) {
        System.out.println("Where put this book?\nWrite a number of place");
        int i = input.nextInt();
        if (i > 20 || i < 1) {
            System.out.println("Wrong number. Must be 1 - 20");
        } else {
            Book newBook = new Book();
            System.out.println("Give me name of book");
            newBook.name = input.next();
            System.out.println("Give me name of author");
            newBook.author = input.next();
            System.out.println("Give me publish year");
            newBook.year = input.next();
            books[i - 1] = newBook;
        }
    }

    private static void bookShow(Scanner input) {
        System.out.println("Which one?\nWrite a number of place");
        int i = input.nextInt();
        if (i > 20 || i < 1) {
            System.out.println("Wrong number. Must be 1 - 20");
        } else {
            if (books[i - 1] == null) {
                System.out.println("Does not exist");
            } else {
                System.out.println(books[i - 1].name + "\n" + books[i - 1].author + "\n" + books[i - 1].year);
            }
        }
    }

    private static void bookShowAll() {
        for (Book book : books) {
            if (book != null) {
                System.out.println(book.name + " " + book.author + " " + book.year + "\n");
            }
        }
    }

    private static void bookDelete(Scanner input) {
        System.out.println("Which one you want to delete\nWrite a number of place");
        int i = input.nextInt();
        if (i > 20 || i < 1) {
            System.out.println("Wrong number. Must be 1 - 20");
        } else {
            if (books[i - 1] == null) {
                System.out.println("Does not exist");
            } else {
                books[i - 1] = null;
            }
        }
    }
}